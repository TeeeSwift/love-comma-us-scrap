import UserApi from '../apis/UserAPI';
// import MediaApi from '../apis/MediaAPI';
import CastingApi from '../apis/CastingAPI';
import ProjectAPI from '../apis/ProjectAPI';
import history from '../history';

// UTILITY
export const setLoaded = Loaded => {
  return {
    type: 'SET_LOADED',
    payload: Loaded
  };
};

export const showHeader = bool => {
  return {
    type: 'TOGGLE_HEADER',
    payload: bool
  };
};

// USER
export const fetchUser = () => {
  return dispatch => {
    UserApi.get('/isLoggedIn', { withCredentials: true }).then(response => {
      if (response.data.auth) {
        dispatch(setUser(response.data.user));
      }
    });
  };
};

export const setUser = user => {
  return {
    type: 'SET_USER',
    payload: user
  };
};

export const resetUser = () => {
  return { type: 'RESET_USER' };
};

// NEW PROJECT
export const initProject = project => {
  return dispatch => {
    ProjectAPI.get('/init', { withCredentials: true }).then(response => {
      dispatch({
        type: 'SET_PROJECT',
        payload: response.data
      });
    });
  };
};

// AUTHENTICATION
export const authenticate = (option, credentials, referral = '') => {
  return dispatch => {
    switch (option) {
      case 'login':
        UserApi.post(
          '/login',
          { ...credentials },
          { withCredentials: true }
        ).then(response => {
          if (response.data.auth === true) {
            dispatch(setLoggedIn('set', response.data));
            dispatch(setUser(response.data.user));
            history.push(referral);
            return;
          }
          // I should set an error somewhere that react can read and display it
          console.log('UH OH');
        });
        return;
      case 'signup':
        UserApi.post(
          '/signup',
          { ...credentials },
          { withCredentials: true }
        ).then(response => {
          console.log(response.data);
          if (!response.data.error) {
            dispatch(setLoggedIn('set', response.data.auth));
            dispatch(setUser(response.data.user));
            history.push(referral);

            return;
          }
          console.log(response.data.message);
        });
        break;
      default:
        return;
    }
  };
};

export const setLoggedIn = (option, boolean) => {
  return dispatch => {
    if (option === 'check') {
      UserApi.get('/isLoggedIn', { withCredentials: true }).then(response => {
        dispatch(setLoggedIn('set', response.data.auth));
      });
      return;
    }

    if (option === 'set') {
      dispatch({
        type: 'SET_LOGGED_IN',
        payload: boolean
      });
      dispatch(setLoaded(true));
      return;
    }
  };
};

export const toggleAuthenticator = toggle => {
  return {
    type: 'TOGGLE_AUTHENTICATOR',
    payload: toggle
  };
};

// MANAGE
export const fetchProjects = query => {
  return dispatch => {
    ProjectAPI.get('/', {
      params: { ...query },
      withCredentials: true
    }).then(response => {
      dispatch(receiveProjects(response.data));
    });
  };
};

export const receiveProjects = projects => {
  return {
    type: 'RECEIVE_PROJECTS',
    payload: projects
  };
};

export const setProjects = data => {
  return dispatch => {
    return new Promise((resolve, reject) => {
      dispatch({ type: 'SET_PROJECTS', payload: data });
      resolve();
    });
  };
};

// Cast Projects

export const fetchCastProjects = () => {
  return (dispatch, getState) => {
    CastingApi.get(`/castProjects/${getState().user._id}`, {
      withCredentials: true
    }).then(response => {
      dispatch(receiveCastProjects(response.data));
    });
  };
};

export const receiveCastProjects = projects => {
  return dispatch => {
    // need to populate these and then set them...
    dispatch({ type: 'RECEIVE_CAST_PROJECTS', payload: projects });
  };
};

export const deleteProject = projectId => {
  return dispatch => {
    ProjectAPI.delete(`/${projectId}`)
      .then(response => {
        dispatch({ type: 'DELETE_PROJECT', payload: projectId });
        dispatch({ type: 'DELETE_CAST_PROJECT', payload: projectId });
      })
      .then(() => history.push('/projects'));
  };
};

export const setError = error => {
  return { type: 'ERROR', payload: error };
};

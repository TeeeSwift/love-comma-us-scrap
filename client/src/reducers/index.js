import { combineReducers } from 'redux';

// UTILITY
const loadedReducer = (Loaded = false, action) => {
  if (action.type === 'SET_LOADED') {
    return action.payload;
  }
  return Loaded;
};

const headerReducer = (show = true, action) => {
  if (action.type === 'TOGGLE_HEADER') {
    return action.payload;
  }
  return show;
};

// USER
const userReducer = (user = {}, action) => {
  if (action.type === 'SET_USER') {
    return action.payload;
  }
  if (action.type === 'RESET_USER') {
    return action.payload;
  }
  return user;
};

// VIEW / MANAGE
const projectsReducer = (projects = [], action) => {
  if (action.type === 'RECEIVE_PROJECTS') {
    return action.payload;
  }
  if (action.type === 'DELETE_PROJECT') {
    return projects.filter(project => project._id !== action.payload);
  }
  if (action.type === 'UPDATE_PROJECT') {
    return projects.map(project => {
      if (project._id === action.payload._id) {
        return {
          ...action.payload,
          coverUrl: project.coverUrl
        };
      }
      return project;
    });
  }

  return projects;
};

const castProjectsReducer = (projects = [], action) => {
  if (action.type === 'RECEIVE_CAST_PROJECTS') {
    return action.payload;
  }

  if (action.type === 'DELETE_CAST_PROJECT') {
    console.log('inside delete cast');
    return projects.filter(project => project.project !== action.payload);
  }

  return projects;
};

//Auth
const loginReducer = (isLoggedIn = null, action) => {
  if (action.type === 'SET_LOGGED_IN') {
    return action.payload;
  }

  return isLoggedIn;
};

const authReducer = (toggle = false, action) => {
  if (action.type === 'TOGGLE_AUTHENTICATOR') {
    return !toggle;
  }

  return toggle;
};

const errorReducer = (error = false, action) => {
  if (action.type === 'ERROR') {
    return action.payload;
  }

  return error;
};

export default combineReducers({
  user: userReducer,
  projects: projectsReducer,
  castProjects: castProjectsReducer,
  isLoggedIn: loginReducer,
  loaded: loadedReducer,
  expandAuth: authReducer,
  showHeader: headerReducer,
  error: errorReducer
});

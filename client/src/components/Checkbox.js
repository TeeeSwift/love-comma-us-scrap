import React from 'react';

const Setting = props => {
  return (
    <div>
      <input
        type="checkbox"
        onChange={e => props.onCheckboxChange(e)}
        name={props.name}
        defaultChecked={props.check}
      />
      <span>No</span>
      <span>Yes</span>
    </div>
  );
};

export default Setting;

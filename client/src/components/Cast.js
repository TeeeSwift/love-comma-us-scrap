import React, { Component } from 'react';

import CastMember from './CastMember';
import AddCast from './AddCast';

import CastingAPI from '../apis/CastingAPI';
import UserAPI from '../apis/UserAPI';

class Cast extends Component {
  state = {
    firstname: '',
    email: '',
    cast: []
  };

  componentDidMount() {
    this.fetchCast(this.props.projectId);
    // Gotta fetch the castings for this project then set it in the state
  }

  fetchCast = projectId => {
    CastingAPI.get(`/${projectId}`, {}, { withCredentials: true }).then(
      response => {
        this.setState({
          cast: [...response.data]
        });
      }
    );
  };

  changeHandler = e => {
    this.setState({
      [e.target.name]: e.target.value
    });
  };

  addCast = e => {
    // ignore the keypress unless it's the enter key
    if (e.which && e.which !== 13) {
      // ignore keypress
      return;
    }

    // Check if the email already exists
    this.state.cast.forEach(person => {
      if (person.user._id === this.state.email) {
        this.setState({
          error: 'email already being used'
        });
      }
    });

    // post their information to casting router
    const data = {
      firstname: this.state.firstname,
      email: this.state.email,
      projectId: this.props.projectId
    };
    CastingAPI.post('/', { ...data }, { withCredentials: true }).then(
      response => {
        if (response.data.hasOwnProperty('error')) {
          console.log('HAS ERROR');
          console.log(response.data);
          return;
        }

        let cast = [...this.state.cast];
        cast.splice(0, 0, response.data);
        this.setState({
          cast,
          firstname: '',
          email: ''
        });
      }
    );

    return;
  };

  handleCastChange = (e, castingId) => {
    e.preventDefault();
    this.setState({
      cast: [
        ...this.state.cast.map(casting => {
          if (casting._id === castingId) {
            return {
              ...casting,
              user: {
                ...casting.user,
                [e.target.name]: e.target.value
              }
            };
          }
          return casting;
        })
      ]
    });
    // CastingAPI.patch(`/${castingId}`).then(response => {
    //   console.log(response);
    // });
  };

  removeCast = castingId => {
    // make delete request with casting id in the params i guess....
    CastingAPI.delete(`/${castingId}`).then(response => {
      console.log(response);
      this.setState({
        cast: [...this.state.cast.filter(casting => casting._id !== castingId)]
      });
    });
  };

  patchCast = user => {
    console.log(user);
    UserAPI.patch(`/${user._id}`, { ...user }).then(response =>
      console.log(response)
    );
  };

  renderCast = cast => {
    if (this.props.editing) {
      return cast.map(member => {
        return (
          <CastMember
            casting={member}
            key={member._id}
            removeCast={this.removeCast}
            editCast={this.handleCastChange}
            patchCast={this.patchCast}
          />
        );
      });
    }

    return cast.map(member => {
      if (member.user._id === this.props.userId) {
        return (
          <div
            key={Math.random()}
            style={{ border: '1px black dotted', padding: '25px' }}
          >
            <h1>YOU ARE THIS CASTING INSTANCE THING!</h1>
            <h2>HERE'S A FUCKING BUTTON SO YOU CAN RECORD SOMETHING</h2>
            <button>RECORD SOMETHING</button>
          </div>
        );
      }
      return (
        <div
          key={member.user._id}
          style={{ border: '1px black dotted', padding: '25px' }}
        >
          <h1>{member.user.firstname}</h1>
          <h3>{member.user.email}</h3>
        </div>
      );
    });
  };

  render() {
    return (
      <div>
        {this.props.editing ? (
          <>
            <h4>Add Cast Member</h4>
            <AddCast
              firstname={this.state.firstname}
              email={this.state.email}
              onChange={this.changeHandler}
              onKeyPress={this.addCast}
            />
            <button onClick={this.addCast}>Add</button>
          </>
        ) : null}
        <p style={{ color: 'red' }}>{this.state.error}</p>
        <h4>Currently called cast</h4>
        {this.renderCast(this.state.cast)}
      </div>
    );
  }
}

export default Cast;

import React, { Component } from 'react';
import { connect } from 'react-redux';

import { fetchProjects, fetchCastProjects } from '../actions';

import ProjectCard from './ProjectCard';

class ManagePage extends Component {
  constructor(props) {
    super(props);
    this.goback = this.goback.bind(this);
  }

  componentDidMount() {
    this.props.fetchProjects({});
    this.props.fetchCastProjects();
    // window.addEventListener('click', this.goback);
  }

  componentWillUnmount() {
    console.log('About to unmount');
    // remove event listener...i guess?
    // window.removeEventListener('click', this.goback);
  }

  goback = e => {
    this.props.history.push('/manage');
  };

  renderProjectCards = projects => {
    return (
      <div className="l-grid-area-projects">
        {projects.map((project, index) => {
          return <ProjectCard key={project._id} projectId={project._id} />;
        })}
      </div>
    );
  };

  render() {
    return (
      <div
        className="manage l-grid l-align-center l-justify-content-center"
        id="OutsideProjectCard"
      >
        <div className="l-grid-area-title">
          <h1>Drafts</h1>
        </div>
        {this.renderProjectCards(this.props.projects)}
        <div>
          <h1>Projects you're cast in</h1>
          {this.renderProjectCards(
            this.props.castProjects.map(entry => {
              return { _id: entry.project };
            })
          )}
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return { projects: state.projects, castProjects: state.castProjects };
};

export default connect(
  mapStateToProps,
  {
    fetchProjects,
    fetchCastProjects
  }
)(ManagePage);

import React, { Component } from 'react';

import { withRouter } from 'react-router-dom';
import UserAPI from '../apis/UserAPI';

class ChangePassword extends Component {
  submitHandler = e => {
    e.preventDefault();
    const formData = new FormData(this.form);
    // validate this stuff
    if (formData.get('newPassword') !== formData.get('newPasswordConfirm')) {
      return console.log('PASSWORDS AINT MATCHIN UP DUDE');
    }
    UserAPI.post('/change-password', formData, {
      withCredentials: true
    }).then(response => {
      if (response.data.hasOwnProperty('message')) {
        console.log('looks like there is an error');
        return console.log(response.data.message);
      }
      this.props.history.push('/');
    });
  };
  render() {
    return (
      <div>
        <h3>reset password</h3>
        <form
          ref={el => {
            this.form = el;
          }}
          onSubmit={e => this.submitHandler(e)}
          id="form"
          encType="multipart/form-data"
        >
          <label htmlFor="oldPassword">old password</label>
          <input
            type="Password"
            name="oldPassword"
            placeholder="old password"
            id="oldPassword"
            required
          />
          <label htmlFor="newPassword">new password</label>
          <input
            type="Password"
            name="newPassword"
            placeholder="new password"
            id="newPassword"
            required
          />
          <br />
          <br />
          <label htmlFor="newPasswordConfirm">new password</label>
          <input
            type="Password"
            name="newPasswordConfirm"
            placeholder="new password"
            id="newPasswordConfirm"
            required
          />
          <br />
          <input type="submit" value="save new password" />
        </form>
      </div>
    );
  }
}

export default withRouter(ChangePassword);

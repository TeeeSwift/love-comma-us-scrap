import React, { Component } from 'react';
import { Router, Route } from 'react-router-dom';
import { connect } from 'react-redux';

// Action
import { fetchUser, setLoggedIn, setLoaded, showHeader } from '../actions';

// Components
import history from '../history';
// import ProjectCard from './ProjectCard';
import CreateWizard from './Create';
import Header from './Header';
import ProtectedRoute from './ProtectedRoute';
import LoginPage from './LoginPage';
import AccountPage from './AccountPage';
import RedeemToken from './RedeemToken';
import ForgotPassword from './ForgotPassword';
import Record from './Record';
import ProjectPage from './ProjectPage';
import Gallery from './Gallery';

class App extends Component {
  componentDidMount() {
    this.props.fetchUser();
    this.props.setLoggedIn('check');
  }

  render() {
    if (this.props.loaded) {
      return (
        <Router history={history}>
          <div className="maxheight">
            {this.props.showHeader ? (
              <Header
                showAuth={this.props.expandAuth}
                isLoggedIn={this.props.isLoggedIn}
              />
            ) : null}
            <div className="l-container">
              <ProtectedRoute
                path="/account"
                component={AccountPage}
                isLoggedIn={this.props.isLoggedIn}
              />
              {this.props.error ? <h1>ERROR</h1> : null}
              <Route path="/login" component={LoginPage} />
              <ProtectedRoute
                path="/create"
                component={CreateWizard}
                isLoggedIn={this.props.isLoggedIn}
              />
              <Route exact path="/forgot-password" component={ForgotPassword} />
              <Route path="/forgot-password/:token" component={RedeemToken} />
              <Route exact path="/record" component={Record} />
              <Route path="/projects" component={ProjectPage} />
              <Route path="/gallery" component={Gallery} />
            </div>
          </div>
        </Router>
      );
    }
    return <div>App is rendering but something wrong bruh</div>;
  }
}

const mapStateToProps = state => {
  return { ...state };
};

export default connect(
  mapStateToProps,
  { setLoggedIn, setLoaded, showHeader, fetchUser }
)(App);

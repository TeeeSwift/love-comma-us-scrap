import React, { Component } from 'react';
import { Link } from 'react-router-dom';

class Nav extends Component {
  render() {
    return (
      <div>
        <Link to={`/`}>
          <button>Home</button>
        </Link>
        <Link to={`/Create`}>
          <button>Create</button>
        </Link>
        <Link to={`/projects`}>
          <button>Projects</button>
        </Link>
        <Link to={`/gallery`}>
          <button>Gallery</button>
        </Link>
      </div>
    );
  }
}

export default Nav;

import React from 'react';

const Subject = props => {
  // If user is creating a new project
  if (props.new) {
    return (
      <div>
        Starring:
        <input
          autoComplete="off"
          type="text"
          placeholder="Who do you want to uplift?"
          name="subject"
          onChange={e => props.onChange(e)}
        />
      </div>
    );
  }

  // if user is editing an existing project
  if (props.editing) {
    return (
      <div>
        Who
        <input
          autoComplete="off"
          defaultValue={props.subject}
          type="text"
          placeholder="name"
          name="subject"
          first="true"
          onChange={e => props.onChange(e, 'text')}
          onBlur={props.save}
        />
      </div>
    );
  }

  // if user is just looking at an existing project and not editing it
  return <h2>{props.subject}</h2>;
};

export default Subject;

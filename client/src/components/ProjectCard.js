import React from 'react';
import { withRouter, Link } from 'react-router-dom';
import { connect } from 'react-redux';

import MediaApi from '../apis/MediaAPI';
import ProjectAPI from '../apis/ProjectAPI';

class ProjectCard extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      loaded: false,
      project: {},
      coverUrl: ''
    };
  }

  // Lifecycle methods
  componentDidMount() {
    // populate
    this.populateProject(this.props.projectId).then(response => {
      this.setState({
        project: { ...response[0] },
        coverUrl: response[1],
        loaded: true
      });
    });
  }

  //
  populateProject = projectId => {
    const content = ProjectAPI.get(`/${projectId}`).then(
      response => response.data
    );
    // populate the cover
    const cover = MediaApi.get(`/image/${projectId}`).then(response => {
      return response.data;
    });

    return Promise.all([content, cover]);
  };

  //rendering stuff
  render() {
    const { coverUrl } = this.state;
    const { subject, description, _id } = this.state.project;

    if (!this.state.loaded) {
      return <div>loading</div>;
    }

    return (
      <Link to={`/projects/${_id}`}>
        <div className="c-project-card">
          <img
            src={coverUrl}
            className="o-media c-project-card__image"
            alt={subject}
          />
          <div className="c-project-card__overlay">
            <div className="c-project-card__wrapper">
              <div className="c-project-card__details">
                <div className="c-project-card__title shout tilt">
                  {subject}
                </div>
                <div className="c-project-card__description tilt talk">
                  {description}
                </div>
              </div>
            </div>
          </div>
        </div>
      </Link>
    );
  }
}

const mapStateToProps = state => {
  return { user: state.user, isLoggedIn: state.isLoggedIn };
};

export default withRouter(
  connect(
    mapStateToProps,
    {}
  )(ProjectCard)
);

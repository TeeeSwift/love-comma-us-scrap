import React from 'react';

const Title = props => {
  // If user is creating a new project
  if (props.new) {
    return (
      <div>
        Project Title:
        <input
          autoComplete="off"
          type="text"
          placeholder="What's the occasion?"
          name="title"
          onChange={e => props.onChange(e)}
        />
      </div>
    );
  }

  // if user is editing an existing project
  if (props.editing) {
    return (
      <div>
        Who
        <input
          autoComplete="off"
          defaultValue={props.title}
          type="text"
          placeholder="name"
          name="title"
          first="true"
          onChange={e => props.onChange(e, 'text')}
          onBlur={props.save}
        />
      </div>
    );
  }

  // if user is just looking at an existing project and not editing it
  return <h2>{props.title}</h2>;
};

export default Title;

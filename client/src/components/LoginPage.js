import React from 'react';
import { connect } from 'react-redux';

import Authenticator from './Authenticator';
import { showHeader } from '../actions';

class LoginPage extends React.Component {
  componentDidMount() {
    this.props.showHeader(false);
  }

  componentWillUnmount() {
    this.props.showHeader(true);
  }

  from = this.props.location.state
    ? this.props.location.state.from
    : { pathname: '/' };

  render() {
    return (
      <div>
        <h2>Please Log in or Sign Up to continue</h2>
        <Authenticator from={this.from} />
      </div>
    );
  }
}

export default connect(
  null,
  { showHeader }
)(LoginPage);

import React, { Component } from 'react';

import UserAPI from '../apis/UserAPI';

class RedeemToken extends Component {
  state = {
    success: false,
    loaded: false,
    done: false
  };

  componentDidMount() {
    console.log('calling api');
    UserAPI.get(`/token/${this.props.match.params.token}`).then(response => {
      this.setState({
        success: response.data.success,
        loaded: true,
        userId: response.data.userId
      });
      console.log(response.data);
    });
  }

  submit = e => {
    e.preventDefault();
    console.log('submitting');
    const formData = new FormData(this.form);

    UserAPI.patch(`/token/${this.props.match.params.token}`, formData, {
      headers: {
        'Content-Type': 'multipart/form-data'
      }
    }).then(response => {
      console.log(response);
      if (response.data.success) {
        console.log(response.data);
        return this.setState({
          done: true
        });
      }
      console.log(response);
    });
  };

  render() {
    return this.state.done ? (
      <div>
        password has been reset, you gucci
        <button>Button1</button>
        <button>Button2</button>
        <button>Button3</button>
        <button>Button4</button>
      </div>
    ) : (
      <div>
        {this.state.loaded ? (
          this.state.success ? (
            <div>
              <h3>Let's Reset your password my guy </h3>
              <form
                ref={el => (this.form = el)}
                onSubmit={e => this.submit(e)}
                encType="multipart/form-data"
              >
                <label htmlFor="email">email</label>
                <input type="email" placeholder="email" name="email" />
                <br />
                <label htmlFor="password">password</label>
                <input type="password" placeholder="password" name="password" />
                <br />
                <label htmlFor="confirmPassword">
                  confirm your new password
                </label>
                <input
                  type="password"
                  placeholder="confirm password"
                  name="confirmPassword"
                />
                <input type="submit" value="RESET PASSWORD" />
              </form>
            </div>
          ) : (
            <div>
              <h3>THIS LINK ISN'T EVEN VALID DUDE</h3>
            </div>
          )
        ) : (
          <div />
        )}
      </div>
    );
  }
}

export default RedeemToken;

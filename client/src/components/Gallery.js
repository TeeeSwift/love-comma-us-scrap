import React, { Component } from 'react';
import { connect } from 'react-redux';
import { fetchProjects } from '../actions';

import ProjectCard from './ProjectCard';
class Gallery extends Component {
  componentDidMount() {
    // fetch the projects where published = true
    this.props.fetchProjects({
      published: true
    });
  }

  render() {
    return this.props.projects.map((project, index) => {
      return <ProjectCard key={project._id} projectId={project._id} />;
    });
  }
}

const mapStateToProps = state => {
  return {
    projects: state.projects
  };
};
export default connect(
  mapStateToProps,
  { fetchProjects }
)(Gallery);

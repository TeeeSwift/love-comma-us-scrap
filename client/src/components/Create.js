import React, { Component } from 'react';

// Components
import ImageUploader from './ImageUploader';
import Subject from './Subject';
import Title from './Title';
import Description from './Description';
import Settings from './Settings';

// Actions
import {} from '../actions';

//APIs
import UploadMedia from '../apis/MediaAPI';
import ProjectAPI from '../apis/ProjectAPI';

class CreateWizard extends Component {
  state = {
    file: null,
    project: {
      owner: null,
      title: null,
      subject: null,
      description: null,
      settings: {
        public: false,
        open: false
      },
      published: false
    },
    filePath: '',
    preview: false
  };

  setFile = e => {
    e.preventDefault();
    if (e.target.files.length === 0) {
      return this.setState({
        file: null,
        filePath: '',
        preview: false
      });
    }
    return this.setState(
      {
        file: e.target.files[0],
        filePath: URL.createObjectURL(e.target.files[0]),
        preview: true
      },
      () => console.log(this.state)
    );
  };

  submitProject = project => {
    // add validation here somehow

    ProjectAPI.post(
      `/`,
      { project: { ...this.state.project } },
      { withCredentials: true }
    ).then(response => {
      console.log(response.data);
      if (this.state.file === null) {
        return this.props.history.push({
          pathname: `/projects/${response.data._id}`,
          state: { editing: true }
        });
      }

      let project_id = response.data._id;

      // upload image
      const formData = new FormData();
      formData.append('file', this.state.file);
      formData.append('projectId', project_id);

      UploadMedia.post('/image/cover', formData, {
        withCredentials: true,
        headers: {
          'Content-Type': 'multipart/form-data'
        }
      }).then(response => {
        console.log('file was sent....');
        console.log(response.data);
        this.props.history.push({
          pathname: `/projects/${project_id}`,
          state: { editing: true }
        });
      });
    });
  };

  projectChangeHandler = e => {
    e.preventDefault();
    this.setState({
      project: {
        ...this.state.project,
        [e.target.name]: e.target.value
      }
    });
  };

  settingsChangeHandler = (e, string, bool) => {
    this.setState({
      project: {
        ...this.state.project,
        settings: {
          ...this.state.project.settings,
          [e.target.getAttribute('name')]: bool
        }
      }
    });
  };

  render() {
    return (
      <div className="l-grid maxheight l-container l-columns-2">
        <div
          className={`create l-grid ${
            this.state.preview ? 'l-grid-active l-align-center' : ''
          }`}
        >
          <div className="l-center l-grid-area-form">
            <h1>Project Information</h1>
            <p>Let's start building mamories for someone you love</p>
            <form onSubmit={e => e.preventDefault()} autoComplete="off">
              <Title new onChange={this.projectChangeHandler} />
              <Subject new onChange={this.projectChangeHandler} />
              <div
                className={`${
                  this.state.preview
                    ? 'l-grid-area-preview l-center'
                    : 'l-center l-justify-bottom l-grid-area-preview'
                }`}
              >
                Cover Photo:
                {this.state.preview ? (
                  <img src={this.state.filePath} alt="preview" />
                ) : null}
                <ImageUploader onchange={this.setFile} id="imageUploader" />
              </div>
              <Description new onChange={this.projectChangeHandler} />
            </form>
          </div>
        </div>
        <div className="l-grid-area-save create__save">
          <Settings
            isOwner
            editing
            save={this.settingsChangeHandler}
            settings={this.state.project.settings}
          />
          <button onClick={this.submitProject}>SAVE PROJECT</button>
        </div>
      </div>
    );
  }
}

export default CreateWizard;

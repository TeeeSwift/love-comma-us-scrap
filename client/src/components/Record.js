import React, { Component } from 'react';

class Record extends Component {
  getMedia = () => {
    navigator.mediaDevices
      .getUserMedia({ video: true, audio: true })
      .then(stream => {
        console.log(stream);
        this.video.srcObject = stream;
      });
  };

  stopMedia = () => {
    const stream = this.video.srcObject;
    if (stream === null) {
      return;
    }
    const tracks = stream.getTracks();

    tracks.forEach(track => {
      track.stop();
    });
  };

  render() {
    return (
      <div style={{ position: 'fixed', top: '500px', left: '400px' }}>
        <button onClick={this.getMedia}>Start camera</button>
        <button onClick={this.stopMedia}>stop camera</button>
        <video autoPlay ref={ref => (this.video = ref)} muted="muted" />
      </div>
    );
  }
}

export default Record;

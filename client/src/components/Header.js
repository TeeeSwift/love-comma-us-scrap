import React from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';

import { toggleAuthenticator } from '../actions/';

import Authenticator from './Authenticator';
import Nav from './Nav';
import ProfileMenu from './ProfileMenu';

const Header = props => {
  if (props.location.pathname === '/login') {
    return null;
  }

  return (
    <div className="l-fixed header">
      {props.isLoggedIn ? (
        <ProfileMenu />
      ) : (
        // <Logout />
        <div>
          <button onClick={props.toggleAuthenticator}>
            {props.showAuth ? 'Close' : 'Login / Signup'}
          </button>
          {props.showAuth ? <Authenticator /> : null}
        </div>
      )}
      <Nav />
    </div>
  );
};

export default connect(
  null,
  { toggleAuthenticator }
)(withRouter(Header));

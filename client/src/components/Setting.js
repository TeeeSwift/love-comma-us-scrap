import React from 'react';

const Setting = props => {
  return (
    <div>
      <span
        className={`setting setting--${!props.check}`}
        name={props.name}
        onClick={e => props.onClick(e, 'setting', false)}
      >
        No
      </span>
      <span
        className={`setting setting--${props.check}`}
        name={props.name}
        onClick={e => props.onClick(e, 'setting', true)}
      >
        Yes
      </span>
    </div>
  );
};

export default Setting;

import React from 'react';

const Description = props => {
  // if user is creating a new project
  if (props.new) {
    return (
      <div>
        Description:
        <textarea
          name="description"
          placeholder="Why do you want to celebrate this person?"
          rows="5"
          cols="40"
          onChange={e => props.onChange(e)}
        />
      </div>
    );
  }

  // if user is editing an existing project
  if (props.editing) {
    return (
      <div>
        Why
        <textarea
          defaultValue={props.description}
          name="description"
          placeholder="Why do you want to celebrate this person?"
          rows="5"
          cols="40"
          onChange={e => props.onChange(e, 'text')}
          onBlur={props.save}
        />
      </div>
    );
  }

  // if user is simply looking at an existing project and not editing it
  return <h2>{props.description}</h2>;
};

export default Description;

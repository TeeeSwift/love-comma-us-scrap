import React from 'react';

import axios from '../apis/UserAPI';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';

import { setLoggedIn, toggleAuthenticator } from '../actions';

const Logout = props => {
  return (
    <button
      onClick={() =>
        axios
          .get('/logout', { withCredentials: true })
          .then(response => props.setLoggedIn('set', false))
          .then(props.toggleAuthenticator(false))
          .then(props.history.push('/'))
      }
    >
      Log Out
    </button>
  );
};

export default withRouter(
  connect(
    null,
    { setLoggedIn, toggleAuthenticator }
  )(Logout)
);

import React, { Component } from 'react';

class ImageUploader extends Component {
  render() {
    return (
      <input name="image" type="file" onChange={e => this.props.onchange(e)} />
    );
  }
}

export default ImageUploader;

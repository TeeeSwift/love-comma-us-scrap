import React from 'react';

import { Link } from 'react-router-dom';
import { connect } from 'react-redux';

// importing components
import Subject from './Subject';
import Description from './Description';
import Settings from './Settings';
import Cast from './Cast';

const DetailPane = props => {
  // if we're editing
  return (
    <div onClick={e => e.stopPropagation()}>
      <div>
        <Link to={'/projects/'}>
          <button onClick={props.toggleDetail}>CLOSE THIS BITCH</button>
        </Link>
        <Subject
          subject={props.project.subject}
          onChange={props.updateProject}
          projectId={props.project._id}
          save={props.patchProject}
        />
        <br />
        <Description
          description={props.project.description}
          projectId={props.project._id}
          onChange={props.updateProject}
          save={props.patchProject}
        />
        <br />
        <Settings
          save={props.updateProject}
          settings={props.project.settings}
          projectId={props.project._id}
        />
        <Cast projectId={props.project._id} />
      </div>
      <button className="color-denote-error" onClick={props.expandDelete}>
        DELETE PROJECT
      </button>
      {props.showDelete ? (
        <div>
          <p>Please enter "delete" and submit to delete this project</p>
          <form onSubmit={e => props.deleteProject(e)}>
            <input
              type="text"
              onChange={e => props.changeHandler(e)}
              name="confirmDelete"
              value={props.confirmDelete}
            />
            <input type="submit" />
          </form>
        </div>
      ) : null}
    </div>
  );
};

export default connect(
  null,
  {
    // updateAndPatch, updateSettingsAndPatch, patchProject
  }
)(DetailPane);

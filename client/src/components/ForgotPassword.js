import React, { Component } from 'react';

import UserAPI from '../apis/UserAPI';

class ForgotPassword extends Component {
  state = {
    email: '',
    success: null
  };

  changeHandler = e => {
    this.setState({
      [e.target.name]: e.target.value
    });
  };

  submit = e => {
    e.preventDefault();
    UserAPI.post('/token', { ...this.state }).then(response => {
      console.log(response);
      if (response.data.success) {
        this.setState({ success: true });
      }
    });
  };

  render() {
    return (
      <div>
        {this.state.success ? (
          <div>
            <h3>SUCCESS</h3>
            <p>
              Your password has been reset and you can redeem it from your
              email: {this.state.email}
            </p>
          </div>
        ) : (
          <>
            <h3>Let's reset your password!!</h3>
            <form onSubmit={e => this.submit(e)}>
              <label htmlFor="email">Email</label>
              <input
                type="email"
                name="email"
                onChange={e => this.changeHandler(e)}
              />
              <input type="submit" value="submit" />
            </form>
          </>
        )}
      </div>
    );
  }
}

export default ForgotPassword;

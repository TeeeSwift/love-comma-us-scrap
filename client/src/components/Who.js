import React from 'react';
import { connect } from 'react-redux';

import { setSubject } from '../actions/';

const Subject = props => {
  return (
    <div>
      Who is this for?
      <input
        type="text"
        placeholder="name"
        name="subject"
        onBlur={e => props.setSubject(e.currentTarget.value)}
        first="true"
      />
    </div>
  );
};

const mapStateToProps = state => {
  return { subject: state.project.subject };
};

export default connect(
  mapStateToProps,
  { setSubject }
)(Subject);

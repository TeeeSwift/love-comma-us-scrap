import React from 'react';
import { connect } from 'react-redux';

import { authenticate } from '../actions';

class Login extends React.Component {
  state = {
    username: '',
    password: ''
  };

  submit = e => {
    e.preventDefault();
    this.props.authenticate(
      'login',
      { ...this.state },
      this.props.from ? this.props.from : ''
    );
  };

  handleInput = e => {
    this.setState({ [e.target.name]: e.target.value });
  };

  render() {
    return (
      <div>
        <form onSubmit={this.submit}>
          <input
            type="text"
            name="username"
            placeholder="username"
            onChange={e => this.handleInput(e)}
            value={this.state.username}
          />
          <input
            type="password"
            name="password"
            placeholder="password"
            onChange={e => this.handleInput(e)}
            value={this.state.password}
          />
          <button onClick={e => this.submit(e)}>Log In</button>
          <a href="/forgot-password">I forgot my password...</a>
        </form>
      </div>
    );
  }
}

export default connect(
  null,
  { authenticate }
)(Login);

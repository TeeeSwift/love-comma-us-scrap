import React, { Component } from 'react';

// Components
import ChangePassword from './ChangePassword';
import Deactivate from './Deactivate';

class AccountPage extends Component {
  render() {
    return (
      <div>
        <h1>account page</h1>
        <ChangePassword />
        <Deactivate />
      </div>
    );
  }
}

export default AccountPage;

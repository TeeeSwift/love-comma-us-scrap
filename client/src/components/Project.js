import React from 'react';
import { withRouter, Link } from 'react-router-dom';
import { connect } from 'react-redux';

import { deleteProject } from '../actions';

import MediaApi from '../apis/MediaAPI';
import CastingApi from '../apis/CastingAPI';
import ProjectAPI from '../apis/ProjectAPI';

import Subject from './Subject';
import Description from './Description';
import Settings from './Settings';
import Cast from './Cast';
import Cover from './Cover';

class Project extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      editing: this.props.editing,
      loaded: false,
      showDelete: false,
      confirmDelete: '',
      project: {},
      castings: [],
      isOwner: null,
      isCast: null,
      CastId: ''
    };
  }

  // Lifecycle methods
  componentDidMount() {
    // populate
    this.populateProject(this.props.match.params.projectId).then(response => {
      this.setState(
        {
          project: { ...response[0] },
          castings: [...response[1]],
          loaded: true,
          isOwner: response[0].owner === this.props.user._id,
          isCast:
            response[1].filter(
              casting => casting.user._id === this.props.user._id
            ).length > 0
        },
        () => {
          console.log(
            `You are ${
              this.state.isOwner ? '' : 'not'
            } the owner of this project`
          );
          console.log(
            `You are ${
              this.state.isCast ? '' : 'not'
            } a cast member of this project`
          );
        }
      );
    });
  }

  // Populate project's content and castings
  populateProject = projectId => {
    const content = ProjectAPI.get(`/${projectId}`).then(
      response => response.data
    );
    // populate castings
    const castings = CastingApi.get(`/${projectId}`).then(
      response => response.data
    );
    return Promise.all([content, castings]);
  };

  // updates state and sends patch request
  updateProject = (e, key, bool = null) => {
    if (key === 'text') {
      return this.setState(
        {
          project: { ...this.state.project, [e.target.name]: e.target.value }
        },
        () => {
          ProjectAPI.patch(`/${this.state.project._id}`, {
            project: this.state.project
          });
        }
      );
    }

    // if we updating the setting
    if (key === 'setting') {
      this.setState(
        {
          project: {
            ...this.state.project,
            settings: {
              ...this.state.project.settings,
              [e.target.getAttribute('name')]: bool
            }
          }
        },
        () => {
          ProjectAPI.patch(`/${this.state.project._id}`, {
            project: this.state.project
          });
        }
      );
    }
  };

  deleteTextHandler = e => {
    this.setState({
      [e.target.name]: e.target.value
    });
  };

  //Delete Project
  deleteProject = e => {
    e.preventDefault();
    if (this.state.confirmDelete === 'delete') {
      return this.props.deleteProject(this.state.project._id);
    }
    return console.log('type in delete dude....');
  };

  render() {
    const { coverUrl } = this.state;
    const { subject, description, _id } = this.state.project;

    if (!this.state.loaded) {
      return <div>loading</div>;
    }

    // if it's not a finished project
    return (
      <div className="project">
        <Cover
          projectId={this.props.match.params.projectId}
          editing={this.state.editing}
        />

        <div>
          <div>
            <Link to={'/projects/'}>
              <button>Back to Projects</button>
            </Link>
            <Subject
              editing={this.state.editing}
              subject={subject}
              onChange={this.updateProject}
              save={this.patchProject}
            />
            <Description
              editing={this.state.editing}
              description={description}
              onChange={this.updateProject}
              save={this.patchProject}
            />

            <Settings
              editing={this.state.editing}
              isOwner={!!this.state.isOwner}
              save={this.updateProject}
              settings={this.state.project.settings}
            />
            <Cast
              projectId={_id}
              editing={this.state.editing}
              userId={this.props.user._id}
            />
          </div>

          {/* Show edit button if user is the owner of the project */}
          {this.state.isOwner ? (
            <>
              <button
                onClick={() => {
                  this.setState({ editing: !this.state.editing });
                }}
              >
                {this.state.editing ? 'DONE EDITING' : 'EDIT THIS SHIT'}
              </button>

              <button
                className="color-denote-error"
                onClick={() => {
                  this.setState({ showDelete: !this.state.showDelete });
                }}
              >
                {this.state.showDelete ? 'Never mind' : 'DELETE PROJECT'}
              </button>
              {this.state.showDelete ? (
                <div>
                  <p>Please enter "delete" and submit to delete this project</p>
                  <form onSubmit={e => this.deleteProject(e)}>
                    <input
                      type="text"
                      onChange={e => this.deleteTextHandler(e)}
                      name="confirmDelete"
                      value={this.state.confirmDelete}
                    />
                    <input type="submit" />
                  </form>
                </div>
              ) : null}
            </>
          ) : null}
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return { user: state.user, isLoggedIn: state.isLoggedIn };
};

export default withRouter(
  connect(
    mapStateToProps,
    { deleteProject }
  )(Project)
);

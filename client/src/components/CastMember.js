import React from 'react';

const CastMember = props => {
  return (
    <div>
      <input
        autoComplete="off"
        type="text"
        value={props.casting.user.firstname}
        onChange={e => props.editCast(e, props.casting._id)}
        onBlur={() => props.patchCast(props.casting.user)}
        name="firstname"
      />
      <input
        autoComplete="off"
        type="email"
        value={props.casting.user.email}
        onChange={e => props.editCast(e, props.casting._id)}
        onBlur={() => props.patchCast(props.casting.user)}
        name="email"
      />

      <button onClick={() => props.removeCast(props.casting._id)}>X</button>
    </div>
  );
};

export default CastMember;

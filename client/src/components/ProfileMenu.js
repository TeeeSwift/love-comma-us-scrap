import React from 'react';

import { Link } from 'react-router-dom';

import Logout from './Logout';

class ProfileMenu extends React.Component {
  state = {
    showMenu: false
  };

  render() {
    return (
      <div>
        <button
          onClick={() => this.setState({ showMenu: !this.state.showMenu })}
        >
          Profile
        </button>
        {this.state.showMenu ? (
          <ul>
            <li>
              <Link
                to={'/account'}
                onClick={() => this.setState({ showMenu: false })}
              >
                Manage account
              </Link>
            </li>
            <li>
              <Logout {...this.props} />
            </li>
          </ul>
        ) : null}
      </div>
    );
  }
}

export default ProfileMenu;

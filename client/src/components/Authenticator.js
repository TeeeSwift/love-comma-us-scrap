import React from 'react';
import Signup from './Signup';
import Login from './Login';

import { connect } from 'react-redux';

import ProfileMenu from './ProfileMenu';

class Authenticator extends React.Component {
  renderAuth = isLoggedIn => {
    return isLoggedIn ? (
      <ProfileMenu {...this.props} />
    ) : (
      <>
        <Signup {...this.props} />
        <Login {...this.props} />
      </>
    );
  };

  render() {
    return this.renderAuth(this.props.isLoggedIn);
  }
}

const mapStateToProps = state => {
  return { isLoggedIn: state.isLoggedIn };
};

export default connect(mapStateToProps)(Authenticator);

import React from 'react';

import Setting from './Setting';

const Settings = props => {
  const createSetting = option => {
    let explanations = {
      public: 'Anybody can watch the final movie',
      open: 'Anybody can submit a video message for this person'
    };

    return (
      <div key={option}>
        <p>{explanations[option]}</p>
        <Setting
          label={option}
          key={option}
          onClick={props.editing ? props.save : () => {}}
          name={option}
          check={props.settings[option]}
        />
      </div>
    );
  };

  if (props.isOwner) {
    return (
      <div>
        <h1>Settings</h1>
        {props.settings && Object.keys(props.settings).map(createSetting)}
      </div>
    );
  }
  return <div />;
};

export default Settings;

import React from 'react';
import { connect } from 'react-redux';

import { authenticate } from '../actions';

class Signup extends React.Component {
  state = {
    username: '',
    email: '',
    password: '',
    firstname: '',
    lastname: ''
  };

  submit = e => {
    e.preventDefault();
    this.props.authenticate(
      'signup',
      { ...this.state },
      this.props.from ? this.props.from : ''
    );
  };

  handleInput = e => {
    this.setState({ [e.target.name]: e.target.value });
  };

  render() {
    return (
      <div>
        <form onSubmit={this.submit}>
          <input
            type="text"
            name="username"
            placeholder="username"
            onChange={e => this.handleInput(e)}
            value={this.state.username}
            required
          />
          <input
            type="text"
            name="firstname"
            placeholder="firstname"
            onChange={e => this.handleInput(e)}
            value={this.state.firstname}
            required
          />
          <input
            type="text"
            name="lastname"
            placeholder="lastname"
            onChange={e => this.handleInput(e)}
            value={this.state.lastname}
          />
          <input
            type="email"
            name="email"
            placeholder="email address"
            onChange={e => this.handleInput(e)}
            value={this.state.email}
            required
          />
          <br />
          <input
            type="password"
            name="password"
            placeholder="password"
            onChange={e => this.handleInput(e)}
            value={this.state.password}
            required
          />
          <button onClick={e => this.submit(e)}>Sign Up</button>
        </form>
      </div>
    );
  }
}

export default connect(
  null,
  { authenticate }
)(Signup);

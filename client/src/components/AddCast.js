import React from 'react';

const AddCast = props => {
  return (
    <>
      <input
        autoComplete="off"
        id="nameInput"
        type="text"
        placeholder="name"
        name="firstname"
        value={props.firstname}
        onChange={e => props.onChange(e)}
        onKeyPress={e => props.onKeyPress(e)}
      />
      <input
        autoComplete="off"
        type="email"
        placeholder="email"
        name="email"
        value={props.email}
        onChange={e => props.onChange(e)}
        onKeyPress={e => props.onKeyPress(e)}
      />
    </>
  );
};

export default AddCast;

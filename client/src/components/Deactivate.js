import React, { Component } from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';

import { setLoggedIn, toggleAuthenticator } from '../actions';

import UserAPI from '../apis/UserAPI';

class Deactivate extends Component {
  state = {
    show: false,
    confirm: ''
  };

  submit = e => {
    e.preventDefault();
    if (this.state.confirm !== 'deactivate') {
      return;
    }
    UserAPI.delete('/', { withCredentials: true }).then(success => {
      console.log(success);
      this.props.toggleAuthenticator(false);
      this.props.setLoggedIn('set', false);
      this.props.history.push('/');
    });
  };

  render() {
    return (
      <div>
        <p>Deactivate</p>
        {this.state.show ? (
          <div>
            <form onSubmit={e => this.submit(e)}>
              <label htmlFor="confirm">
                Type 'deactivate' and submit to confirm deactivation
              </label>
              <br />
              <input
                type="text"
                onChange={e => {
                  this.setState({ [e.target.name]: e.target.value }, () => {
                    console.log(this.state.confirm);
                  });
                }}
                name="confirm"
                value={this.state.confirm}
                placeholder="deactivate"
              />
              {this.state.confirm === 'deactivate' ? (
                <input type="submit" value="confirm" />
              ) : null}
            </form>
          </div>
        ) : null}
        <button onClick={() => this.setState({ show: !this.state.show })}>
          {this.state.show ? 'wait nvm, close this' : ' deactivate my account '}
        </button>
      </div>
    );
  }
}

export default withRouter(
  connect(
    null,
    { setLoggedIn, toggleAuthenticator }
  )(Deactivate)
);

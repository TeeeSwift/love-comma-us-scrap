import React, { Component } from 'react';

import MediaAPI from '../apis/MediaAPI';

class Cover extends Component {
  state = {
    coverUrl: '',
    filePath: '',
    showPreview: false,
    file: null
  };

  componentDidMount() {
    MediaAPI.get(`/image/${this.props.projectId}`).then(response =>
      this.setState({ coverUrl: response.data })
    );
  }

  // clicking on div fires click on image input
  uploadHandler = e => {
    e.stopPropagation();
    this.imgupload.click();
  };

  storeFile = e => {
    if (e.target.files.length > 0) {
      this.setState({
        file: e.target.files[0],
        filePath: URL.createObjectURL(e.target.files[0]),
        showPreview: true
      });
    }
  };

  savePhoto = () => {
    const formData = new FormData();
    formData.append('file', this.state.file);
    formData.append('projectId', this.props.projectId);

    MediaAPI.post('/image/cover', formData, {
      withCredentials: true,
      headers: {
        'Content-Type': 'multipart/form-data'
      }
    }).then(response => {
      console.log(response.data);
      this.setState({ showPreview: false });
    });
  };

  renderImage = url => {
    return (
      <img
        src={url}
        className="o-media c-project-card__image"
        alt={Math.random()}
      />
    );
  };

  // shows options for saving the new cover photo or cancelling
  renderPreviewOptions = () => {
    return this.state.showPreview ? (
      <div>
        <button
          onClick={() => {
            this.imgupload.value = '';
            this.setState({ filePath: '', file: '', showPreview: false });
          }}
        >
          Cancel
        </button>
        <button onClick={this.savePhoto}>Save</button>
      </div>
    ) : null;
  };

  // {this.renderImage(
  //   this.state.filePath === ''
  //     ? this.state.coverUrl
  //     : this.state.filePath
  // )}
  render() {
    return (
      <>
        <div
          style={{
            backgroundImage: `url(${
              this.state.filePath === ''
                ? this.state.coverUrl
                : this.state.filePath
            })`,
            backgroundRepeat: 'no-repeat',
            backgroundSize: 'contain',
            position: 'relative',
            minHeight: '95vh'
          }}
        >
          <div>
            <input
              type="file"
              id="imgupload"
              style={{ display: 'none' }}
              ref={ref => (this.imgupload = ref)}
              onInput={e => this.storeFile(e)}
            />
          </div>
          <div
            style={{
              position: 'absolute',
              bottom: '5px',
              left: '50%',
              transform: 'translateX(-50%)'
            }}
          >
            {this.renderPreviewOptions()}
            {this.props.editing ? (
              <button onClick={this.uploadHandler} style={{}}>
                Change photo
              </button>
            ) : null}
          </div>
        </div>
      </>
    );
  }
}

export default Cover;

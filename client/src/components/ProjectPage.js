import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Route, Switch } from 'react-router-dom';

import { fetchProjects, fetchCastProjects } from '../actions';

import Project from './Project';
import ProjectCard from './ProjectCard';

class ProjectPage extends Component {
  constructor(props) {
    super(props);
  }

  componentDidMount() {
    this.props.fetchProjects({ published: false });
    this.props.fetchCastProjects({ published: false });
  }

  componentWillUnmount() {
    console.log('About to unmount');
  }

  renderProjectCards = projects => {
    return (
      <div className="l-grid-area-projects">
        {projects.map((project, index) => {
          return <ProjectCard key={project._id} projectId={project._id} />;
        })}
      </div>
    );
  };

  render() {
    return (
      <Switch>
        <Route
          exact
          path="/projects"
          render={() => {
            return (
              <div
                className="manage l-grid l-align-center l-justify-content-center"
                id="OutsideProjectCard"
              >
                <div className="l-grid-area-title">
                  <h1>Drafts</h1>
                </div>
                {this.renderProjectCards(this.props.projects)}
                <div>
                  <h1>Projects you're cast in</h1>
                  {this.renderProjectCards(
                    this.props.castProjects.map(entry => {
                      return { _id: entry.project };
                    })
                  )}
                </div>
              </div>
            );
          }}
        />
        <Route
          exact
          path={`${this.props.match.url}/:projectId`}
          render={props => (
            <Project
              editing={
                this.props.location.state &&
                this.props.location.state.hasOwnProperty('editing')
                  ? true
                  : false
              }
            />
          )}
        />
      </Switch>
    );
  }
}

const mapStateToProps = state => {
  return {
    projects: state.projects,
    castProjects: state.castProjects,
    user: state.user
  };
};

export default connect(
  mapStateToProps,
  {
    fetchProjects,
    fetchCastProjects
  }
)(ProjectPage);

const express = require('express');
// const bodyParser = require('body-parser');
// const cors = require('cors');
const Project = require('../models/projectModel');
const Cover = require('../models/coverModel');
const Casting = require('../models/castingModel');

const router = express.Router();

const populate = project =>
  new Promise((resolve, reject) => {
    Cover.find({ project: project._id })
      .sort('-date')
      .exec()
      .then(cover => {
        console.log(cover);
        const populatedProject = {
          ...project.toObject(),
          coverUrl:
            cover.length > 0 ? cover[0].url : 'https://i.imgur.com/B1rVXnI.jpg'
        };
        resolve(populatedProject);
      });
  });

// Retrieving in bulk
router.route('/').get((req, res) => {
  let Query = { owner: req.user._id };
  // If they sent with extra query parameters, inject them here
  if (req.query) {
    Query = { ...Query, ...req.query };
  }
  Project.find({ ...Query }, { _id: 1 }, (err, foundProjects) => {
    res.send(foundProjects);
  });
});

// Get single project
router.route('/:projectId').get((req, res) => {
  Project.findById(req.params.projectId, (err, project) => {
    res.json(project);
  });
});

// Creating a new project
router.route('/').post((req, res) => {
  const project = new Project({
    ...req.body.project,
    owner: req.user._id
  });
  project.save();
  res.send(project);
});

// Update single project
router.route('/:projectId').patch((req, res) => {
  console.log(req.params);
  Project.findOneAndUpdate(
    { _id: req.params.projectId },
    req.body.project,
    { new: true },
    (err, updatedProject) => {
      if (err) {
        console.log(err);
      } else {
        console.log(`document updated to ${updatedProject}`);
        res.send(updatedProject);
      }
    }
  );
});

// Delete single project
router.route('/:projectId').delete((req, res) => {
  console.log('received delete request for projectId: ', req.params.projectId);
  // delete project
  Project.deleteOne({ _id: req.params.projectId }).then(project => {
    console.log('inside deleteOne function');
    console.log(project);
    res.send('project deleted');
  });

  // delete castings associated with project id

  Casting.deleteMany({ project: req.params.projectId }).then(success =>
    console.log(`deleted ${success}`)
  );
  // delete cover associated with project id
  Cover.deleteMany({ project: req.params.projectId }).then(success =>
    console.log(`deleted ${success}`)
  );
});

// Instantiating new one
router.route('/init').get((req, res) => {
  Project.create(
    {
      owner: req.user._id
    },
    (error, project) => {
      return res.send(project);
    }
  );
});

module.exports = router;

const express = require('express');
const randomWords = require('random-words');

const User = require('../models/userModel');
const Casting = require('../models/castingModel');

const router = express.Router();

// Retrieve castings in bulk
router.route('/castProjects/:userId').get((req, res) => {
  Casting.find({ user: req.params.userId }, { project: 1 }).then(response =>
    // need to populate these projects.....
    // maybe not lol
    res.json(response)
  );
});

router.route('/:projectId').get((req, res) => {
  Casting.find({ project: req.params.projectId }, (error, castings) => {
    const option = [{ path: 'user' }];

    const promise = Casting.populate(castings, option);
    promise.then(response => {
      console.log(response);
      res.send(response);
    });
  });
});

// Create single Casting
router.route('/').post((req, res, next) => {
  // IF THE USER EXISTS
  User.findOne({ email: req.body.email }, async (error, user) => {
    if (error) {
      console.log('there was an error upon searching for email');
      console.log(error);
      res.send(error);
    }
    if (user !== null) {
      console.log('found a user!');
      console.log('here is the user');
      console.log(user);
      console.log('here is the user id');
      console.log(user._id);
      const checkExistingCasting = Casting.findOne({
        user: user._id,
        project: req.body.projectId
      });
      const response = await checkExistingCasting.then(
        found => {
          if (found !== null) {
            console.log(
              'There is already a casting for this person and project'
            );
            console.log(found);
            return {
              error: true,
              message: 'There is already a casting for this person and project'
            };
          }
          // THERE'S A CASTING ALREADY FOR THIS PERSON!!!
          // Send an error and return out of the whole flow
          console.log('There is no casting for thie combination yet');
          console.log(found);
          return { error: false };
          // Throw an error and pass along? idk
        },
        error => {
          return { error: true, message: error.message };
        }
      );
      console.log('here is what the response looks like after checking');
      console.log(response);
      if (response.error) {
        return res.status(200).send(response);
      }

      console.log('If the casting exists, you should not see this');

      // If you found the user but a casting doesn't exist, we just need to make a new casting
      Casting.create({
        user: user._id,
        project: req.body.projectId
      }).then(casting => {
        Casting.populate(casting, { path: 'user' }, (error, casting) => {
          console.log('Made a new casting, populated it, and sending it back');
          return res.send(casting);
        });
      });
    }

    console.log('this should only be showing if the user doesnt exist');

    const temporaryPassword = 'hello';
    randomWords({ exactly: 4, join: '-' });
    User.register(
      {
        firstname: req.body.firstname,
        username: req.body.email,
        email: req.body.email
      },
      temporaryPassword,
      (error, newUser) => {
        console.log(newUser);
        Casting.create({
          user: newUser._id,
          project: req.body.projectId
        }).then(casting => {
          Casting.populate(casting, { path: 'user' }, (error, casting) => {
            res.send(casting);
          });
        });
      }
    );
  });
});

router.route('/:CastingId').delete((req, res, next) => {
  // Find the casting with the project and id and delete it
  console.log(req.params);
  Casting.deleteOne({ _id: req.params.CastingId }, err => next(err));
  res.send('successfully deleted');
});

module.exports = router;

const express = require('express');
const passport = require('passport');
const multer = require('multer');
const crypto = require('crypto');
const nodemailer = require('nodemailer');
const fs = require('fs');
const ejs = require('ejs');

const User = require('../models/userModel');

const router = express.Router();
const upload = multer();

// set up nodemailer
const transporter = nodemailer.createTransport({
  service: 'gmail',
  auth: {
    user: 'TeeeSwift0111',
    pass: 'vTx06hFEuJ7r8uaU7J0f'
  }
});

const mailOptions = async (email, token) =>
  new Promise(async (resolve, reject) => {
    resolve({
      from: 'TeeeSwift0111@gmail.com',
      to: email,
      subject: 'password reset request',
      html: await ejs.renderFile('./emails/forgot-password.ejs', {
        token
      })
    });
  });

router.route('/signup').post((req, res) => {
  User.register(
    new User({
      username: req.body.username,
      email: req.body.email,
      firstname: req.body.firstname,
      lastname: req.body.lastname
    }),
    req.body.password,
    (err, user) => {
      if (err) {
        console.log(err);
        let response = {
          error: true,
          message: err.message
        };
        return res.send(response);
      }
      passport.authenticate('local')(req, res, function() {
        console.log('finished registration');
        res.send({
          auth: req.isAuthenticated(),
          user: req.user
        });
      });
    }
  );
});

router.route('/login').post(passport.authenticate('local'), (req, res) => {
  res.send({
    auth: req.isAuthenticated(),
    user: req.user
  });
});

router.route('/logout').get((req, res) => {
  req.logout();
  res.send(req.isAuthenticated());
});

router.route('/isLoggedIn').get((req, res) => {
  res.send({
    auth: req.isAuthenticated(),
    user: req.user
  });
});

router.route('/change-password').post(upload.any(), (req, res) => {
  const { username } = req.user;
  const { oldPassword, newPassword } = req.body;
  // validate and return errors if things are missing or wrong

  User.findByUsername(username).then(user => {
    user.changePassword(oldPassword, newPassword, (error, newUser) => {
      if (error) {
        return res.send(error);
      }
      return res.send(newUser);
    });
  });
});

router.route('/token/:token').patch(upload.any(), (req, res) => {
  console.log('token patch');
  console.log(req.body);
  console.log(req.params.token);
  User.findOne({
    email: req.body.email,
    token: req.params.token,
    tokenExpiration: { $gt: new Date() }
  }).then(user => {
    if (user !== null) {
      console.log('Found a user with that token');
      console.log(user);
      user.token = '';
      user.tokenExpiration = null;
      user.setPassword(req.body.password, (error, newUser) => {
        if (error) {
          res.json({ success: false, message: error.message });
        }
        newUser.save();
        res.json({ success: true, data: newUser });
      });
      return;
    }
    console.log('User with that token doesnt exist....');
    return res.json({
      success: false,
      message: 'User with that token doesnt exist...'
    });
  });
});

router.route('/token/:token').get((req, res) => {
  User.find({
    token: req.params.token,
    tokenExpiration: { $gt: new Date() }
  }).then(user => {
    if (user.length > 0) {
      console.log('Found a user with that token');
      console.log(user);
      return res.json({ success: true, userId: user[0]._id });
    }
    return res.json({ success: false, userId: null });
  });
});

router.route('/token').post((req, res) => {
  User.findOne({
    email: req.body.email
  }).then(async user => {
    if (user !== null) {
      const now = new Date();
      user.token = crypto.randomBytes(21).toString('hex');
      user.tokenExpiration = now.setDate(now.getDate() + 7);
      user.save();
      transporter.sendMail(await mailOptions(req.body.email, user.token));
      return res.json({
        success: true,
        message: 'created a token, check your email'
      });
    }
    return res.json({ success: false, message: 'didnt find anyone...' });
  });
});

router.route('/:userId').patch(upload.any(), (req, res) => {
  User.findOneAndUpdate(
    { _id: req.params.userId },
    { ...req.body, token: '', tokenExpiration: new Date() },
    {
      new: true
    }
  ).then(response => {
    console.log(response);
    return res.json({
      success: true,
      message: 'successfully updated your password'
    });
  });
});

router.route('/').delete((req, res) => {
  User.deleteOne({ _id: req.user._id }, (error, user) => {
    req.logout();
    res.send('success');
  });
});

module.exports = router;

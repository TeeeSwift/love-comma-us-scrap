const express = require('express');
const multer = require('multer');
const cloudinary = require('cloudinary');
const cloudinaryStorage = require('multer-storage-cloudinary');

const Cover = require('../models/coverModel');
const Project = require('../models/projectModel');

const router = express.Router();

cloudinary.config({
  cloud_name: 'dua0yjmnn',
  api_key: '156445747679935',
  api_secret: 'lG5-f8yUsDb-LwgdnCpE9Sdt1Wk'
});

const storage = cloudinaryStorage({
  cloudinary,
  folder: 'cover',
  allowedFormats: ['jpg', 'png'],
  transformation: [{ width: 500, height: 500, crop: 'limit' }]
});

const upload = multer({ storage });

// Create new cover
router.route('/image/cover').post(
  upload.any(),
  // single('file'),
  (req, res) => {
    cloudinary.v2.uploader.upload(req.files[0].url, (error, response) => {
      console.log('successfully uploaded!! Here is the response', response);
      Cover.create(
        {
          url: response.url,
          owner: req.user._id,
          project: req.body.projectId
        },
        (error, success) => {
          if (error) {
            return res.send('uh oh');
          }
          console.log('in the create callback');
          console.log(success);
          res.send('successfully uploaded and cover created');
        }
      );
    });
  }
);

// Retrieve Single Cover
router.route('/image/:projectId').get((req, res) => {
  Cover.find({ project: req.params.projectId })
    .sort('-date')
    .exec()
    .then(cover => {
      if (cover.length === 0) {
        return res.send('https://i.imgur.com/B1rVXnI.jpg');
      }
      console.log(cover);

      res.send(cover[0].url);
    });
});

// Create Single Cover
router.route('/cover').post((req, res) => {
  Cover.create({
    url: req.body.url,
    owner: req.user._id,
    project: req.body.project
  }).then(cover => {
    console.log(cover);
    res.send(cover);
  });
});

// Delete Single Cover
// We don't delete covers, just create new ones that are retrieved
module.exports = router;

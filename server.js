const mongoose = require('mongoose');
const express = require('express');
const bodyParser = require('body-parser');
const session = require('express-session');
const passport = require('passport');
const LocalStrategy = require('passport-local').Strategy;
const morgan = require('morgan');
const cors = require('cors');

// ROUTES
const projectRouter = require('./routes/projectRouter');
const userRouter = require('./routes/userRouter');
const mediaRouter = require('./routes/mediaRouter');
const castingRouter = require('./routes/castingRouter');

//Models
const User = require('./models/userModel');
const { seed } = require('./seed');

mongoose.Promise = Promise;
mongoose.connect(
  'mongodb://TeeeSwift:GbxNUGtU9EZyAXK@ds115595.mlab.com:15595/love-us',
  async (err, db) => {
    console.log('connected to database');
    db.collection('users').deleteMany({});
    db.collection('projects').deleteMany({});
    db.collection('castings').deleteMany({});
    db.collection('covers').deleteMany({});
    console.log('deleted a bunch of collections');
    seed();
  }
);

const app = express();
const port = process.env.PORT || 3001;

// Parsing Middleware
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(morgan('combined'));

// CORS stuff
const allowedOrigins = ['http://localhost:3000'];
app.use(
  cors({
    credentials: true,
    origin: function(origin, callback) {
      // allow requests with no origin (like mobile apps or curl requests)
      if (!origin) return callback(null, true);
      if (allowedOrigins.indexOf(origin) === -1) {
        var msg =
          'The CORS policy for this site does not ' +
          'allow access from the specified Origin.';
        return callback(new Error(msg), false);
      }
      return callback(null, true);
    }
  })
);

app.use(
  session({
    secret: 'never-alone',
    resave: false,
    saveUninitialized: false
  })
);
app.use(passport.initialize());
app.use(passport.session());
passport.use(User.createStrategy());
passport.serializeUser(User.serializeUser());
passport.deserializeUser(User.deserializeUser());

app.get('/', (req, res) => {
  res.send('yes SIRRRRRRRRRRRRRR');
});

// Routing Middleware
app.use('/projects', projectRouter);
app.use('/user', userRouter);
app.use('/media', mediaRouter);
app.use('/casting', castingRouter);

app.listen(port, () => {
  console.log(`http://localhost:${port}`);
});

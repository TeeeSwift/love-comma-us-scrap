const mongoose = require('mongoose');

const User = require('./models/userModel');
const Project = require('./models/projectModel');
const Cover = require('./models/coverModel');
const Casting = require('./models/castingModel');

const users = [
  {
    username: 'taylor',
    email: 'chung.taylor1@gmail.com',
    password: 'taylor',
    firstname: 'Taylor',
    lastname: 'Chung',
    token: 'token',
    tokenExpiration: new Date('2 dec 2019 3:30:00')
  },
  {
    username: 'sen',
    email: 'sen@sen.com',
    password: 'sen',
    firstname: 'Seneca',
    lastname: 'Trott'
  }
];

const SenProjects = [
  {
    subject: 'Eric',
    description: 'Eric is a great guy',
    settings: {
      public: true,
      open: false
    },
    coverUrl:
      'https://scontent-iad3-1.xx.fbcdn.net/v/t1.0-9/12985623_10107393800416615_1585551674452309013_n.jpg?_nc_cat=101&_nc_ht=scontent-iad3-1.xx&oh=12cd3a290dac972011124cb6e1679cdb&oe=5CF56F35',
    published: true
  },
  {
    subject: 'Vivek',
    description: 'Vivek is a great guy',
    settings: {
      public: true,
      open: false
    },
    coverUrl: 'http://robbwolf.com/wp-content/uploads/2016/05/Vivek.jpg',
    published: false
  },
  {
    subject: 'Mike',
    description: 'Mike is a great guy',
    settings: {
      public: true,
      open: false
    },
    coverUrl:
      'https://scontent-iad3-1.xx.fbcdn.net/v/t1.0-9/49459900_10103409553439766_7491438358917283840_o.jpg?_nc_cat=107&_nc_ht=scontent-iad3-1.xx&oh=87b158602a9a3889114cd5ef39f61035&oe=5CF75667',
    published: false
  }
];

const TaylorProjects = [
  {
    subject: 'Saarik',
    description: 'Saarik is a great guy',
    settings: {
      public: true,
      open: false
    },
    coverUrl:
      'https://scontent-iad3-1.xx.fbcdn.net/v/t1.0-9/11011683_10204467624517349_8548784290306914507_n.jpg?_nc_cat=101&_nc_ht=scontent-iad3-1.xx&oh=34eb75ea7bb4e890a979b62ec958d91d&oe=5CEEBB98',
    published: false
  },
  {
    subject: 'Sameer',
    description: 'Sameer is a great guy',
    settings: {
      public: true,
      open: false
    },
    coverUrl:
      'https://scontent-iad3-1.xx.fbcdn.net/v/t1.0-9/25827_1233183356414_559417_n.jpg?_nc_cat=111&_nc_ht=scontent-iad3-1.xx&oh=0f60970568db5b6da42303d116328d22&oe=5CDCA73://scontent-iad3-1.xx.fbcdn.net/v/t1.0-9/25827_1233183356414_559417_n.jpg?_nc_cat=111&_nc_ht=scontent-iad3-1.xx&oh=0f60970568db5b6da42303d116328d22&oe=5CDCA73F',
    published: false
  },
  {
    subject: 'Kevin',
    description: 'Kevin is a great guy',
    settings: {
      public: true,
      open: false
    },
    coverUrl:
      'https://scontent-iad3-1.xx.fbcdn.net/v/t1.0-9/521631_10151083495687750_257861671_n.jpg?_nc_cat=110&_nc_ht=scontent-iad3-1.xx&oh=19fc9007b868cc140ae3daf973b2a918&oe=5CF07F1A',
    published: false
  },
  {
    subject: 'John',
    description: `Tony's wedding`,
    settings: {
      public: false,
      open: false
    },
    coverUrl:
      'https://scontent-iad3-1.xx.fbcdn.net/v/t1.0-9/53320057_10161586291490008_591718189830766592_o.jpg?_nc_cat=102&_nc_ht=scontent-iad3-1.xx&oh=26de96bb7d72f0b7016e9e23206424b9&oe=5D462906',
    published: true
  }
];

const seed = async () => {
  return new Promise(async (resolve, reject) => {
    const produceUsers = users.map(
      user =>
        new Promise((resolve, reject) => {
          User.register(
            {
              username: user.username,
              email: user.email,
              firstname: user.firstname,
              lastname: user.lastname,
              token: user.token,
              tokenExpiration: user.tokenExpiration
            },
            user.password
          ).then(newUser => resolve(newUser));
        })
    );
    let createdUsers = [];
    await Promise.all(produceUsers).then(response => {
      createdUsers = response;
    });
    // users have now been dumped into an array called 'userArray'
    const produceSenProjects = SenProjects.map(
      project =>
        new Promise((resolve, reject) => {
          Project.create({
            ...project,
            owner: createdUsers[1]._id
          }).then(newProject => {
            Cover.create({
              project: newProject._id,
              owner: createdUsers[1]._id,
              url: project.coverUrl
            });
            resolve(newProject);
          });
        })
    );
    let createdSenProjects = [];
    await Promise.all(produceSenProjects).then(response => {
      createdSenProjects = response;
    });
    console.log(createdSenProjects);

    const produceTaylorProjects = TaylorProjects.map(
      project =>
        new Promise((resolve, reject) => {
          Project.create({
            ...project,
            owner: createdUsers[0]._id
          }).then(newProject => {
            Cover.create({
              project: newProject._id,
              owner: createdUsers[0]._id,
              url: project.coverUrl
            });
            resolve(newProject);
          });
        })
    );
    let createdTaylorProjects = [];
    await Promise.all(produceTaylorProjects).then(response => {
      createdTaylorProjects = response;
    });
    console.log('SEEDED PROJECTS');
    console.log(createdTaylorProjects, createdSenProjects, createdUsers);
    const Projects = [...createdTaylorProjects, ...createdSenProjects];
    Projects.forEach(project => {
      Casting.create({
        project: project._id,
        user: createdUsers[0]._id
      });
      Casting.create({
        project: project._id,
        user: createdUsers[1]._id
      });
    });

    resolve();
  });
  // RESOLVE AFTER THE LAST PROMISE AWAIT
};

module.exports = { seed };

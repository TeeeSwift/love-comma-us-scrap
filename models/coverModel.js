const mongoose = require('mongoose');

const { Schema } = mongoose;

const CoverSchema = new Schema({
  project: String,
  owner: { type: mongoose.Schema.Types.ObjectId, ref: 'User' },
  url: String,
  date: { type: Date, default: Date.now }
});

const Cover = mongoose.model('Cover', CoverSchema);

module.exports = Cover;

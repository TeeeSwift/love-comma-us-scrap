const mongoose = require('mongoose');

const { Schema } = mongoose;
const projectModel = new Schema({
  owner: { type: mongoose.Schema.Types.ObjectId, ref: 'User' },
  title: String,
  subject: String,
  description: String,
  settings: {
    public: {
      type: Boolean,
      default: false
    },
    open: {
      type: Boolean,
      default: false
    }
  },
  published: {
    type: Boolean,
    default: false
  }
});

module.exports = mongoose.model('project', projectModel);

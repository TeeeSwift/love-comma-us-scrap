const mongoose = require('mongoose');

const { Schema } = mongoose;

const CastingSchema = new Schema({
  user: {
    type: Schema.Types.ObjectId,
    ref: 'user'
  },
  project: {
    type: Schema.Types.ObjectId,
    ref: 'project'
  },
  hasFile: {
    type: Boolean,
    default: false
  }
});

const Casting = mongoose.model('casting', CastingSchema);

module.exports = Casting;

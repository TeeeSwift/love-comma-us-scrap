const mongoose = require('mongoose');
const passportLocalMongoose = require('passport-local-mongoose');

const { Schema } = mongoose;

const UserSchema = new Schema({
  username: {
    type: String
  },
  firstname: {
    type: String
  },
  lastname: {
    type: String
  },
  email: {
    type: String,
    required: true
  },
  avatar: {
    type: String
  },
  activated: {
    type: Boolean,
    default: false
  },
  token: {
    type: String
  },
  tokenExpiration: {
    type: Date
  }
});

UserSchema.plugin(passportLocalMongoose, {
  usernameQueryFields: ['username', 'email']
});

const User = mongoose.model('user', UserSchema);

module.exports = User;
